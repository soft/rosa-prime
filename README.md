ROSA PRIME
----------

Provides GPU (nvidia/intel) selection for NVIDIA Optimus laptops.

You need to select PRIME in XFdrake first.

Switch PRIME On:

    sudo rosa-prime-select nvidia

Switch PRIME Off:

    sudo rosa-prime-select intel

On boot it's defaulted to Off (intel).

This project is a fork of FedoraPrime project.
