INSTALL_DIR = /etc/rosa-prime

.PHONY: all install uninstall

all: install

install:
	mkdir -p $(INSTALL_DIR)
	cp ./xorg.conf.template $(INSTALL_DIR)/xorg.conf.template
	cp ./xinitrc.nvidia $(INSTALL_DIR)/xinitrc.nvidia
	cp ./xinitrc.nouveau $(INSTALL_DIR)/xinitrc.nouveau
	cp ./rosa-prime-select /usr/sbin/rosa-prime-select
	cp rosa-prime.service /usr/lib/systemd/system/rosa-prime.service
	systemctl enable rosa-prime.service

uninstall:
	systemctl disable rosa-prime.service
	rm -rf $(INSTALL_DIR)
	rm -f /usr/sbin/rosa-prime-select
	rm -f /usr/lib/systemd/system/rosa-prime.service
	rm -f /etc/X11/xinit/xinitrc.d/nvidia
	rm -f /etc/X11/xinit/xinitrc.d/nouveau
